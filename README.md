# README #

### What is this repository for? ###

* Load profile + Load sample for evaluating the MediaWiki application under different Cloud deployment scenarios. 
* v1.0

### How do I get set up? ###

* Download Apache Jmeter >= 2.9
* Load sample load profile
* Configure user variables
* Run!!

### Who do I talk to? ###

* Santiago Gomez Saez: santiago.gomez-saez@iaas.uni-stuttgart.de